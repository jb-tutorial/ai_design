import os

import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from xgboost import XGBClassifier
from flask import Flask, request, jsonify

app = Flask(__name__)

dirname = os.path.dirname(__file__)
excel_file = os.path.join(dirname, r'module/data_211221.xlsx')
df = pd.read_excel(excel_file)

csv_file = os.path.join(dirname, r'module/recom_code.csv')
recommend_code_map = pd.read_csv(csv_file)

# 내스타일 제안
def suggest_model():
    target_suggest = df['rate1'] > 0.3
    x_data_suggest = df.loc[target_suggest, 'PR_ST':'FA_CO']
    y_data_suggest = df.loc[target_suggest, 'suggest']

    x_train_suggest, x_test_suggest, y_train_suggest, y_test_suggest = train_test_split(
        x_data_suggest,
        y_data_suggest,
        test_size=0.2, shuffle=True, random_state=20)

    xgb = XGBClassifier(n_estimators=50, max_depth=3)
    xgb.fit(x_train_suggest, y_train_suggest)

    y_xgb_pred = xgb.predict(x_test_suggest)
    # print('내 스타일 예측값:', y_xgb_pred)

    xgb_acc = accuracy_score(y_test_suggest, y_xgb_pred)
    print('Accuracy:%.4f' % xgb_acc)

    return xgb


# 특별한 제안
def onemore_model():
    target_onemore = df['rate2'] > 0.5
    x_data_onemore = df.loc[target_onemore, 'PR_ST':'FA_CO']
    y_data_onemore = df.loc[target_onemore, 'onemore']

    x_train_onemore, x_test_onemore, y_train_onemore, y_test_onemore = train_test_split(
        x_data_onemore,
        y_data_onemore,
        test_size=0.2, shuffle=True, random_state=20)

    xgb = XGBClassifier(n_estimators=50, max_depth=3)
    xgb.fit(x_train_onemore, y_train_onemore)

    y_xgb_pred = xgb.predict(x_test_onemore)
    # print('특별한 제안 예측값:', y_xgb_pred)

    xgb_acc = accuracy_score(y_test_onemore, y_xgb_pred)
    print('Accuracy:%.4f' % xgb_acc)
    return xgb


smodel = suggest_model()
omodel = onemore_model()


@app.route('/api/recommend', methods=['POST'])
def api_recommend():
    params = request.get_json()
    print('params %s \n' % params)

    df_request = pd.DataFrame.from_dict(params)
    print('df_request %s ' % df_request)
    print()

    suggest_pred = smodel.predict(df_request)
    print('내 스타일 예측값:', suggest_pred)

    onemore_pred = omodel.predict(df_request)
    print('특별한 제안 예측값:', onemore_pred)
    print()

    print('suggest_pred ', int(suggest_pred[0]))
    print('onemore_pred ', int(onemore_pred[0]))

    suggest_code = recommend_code_map.loc[recommend_code_map['AICODE'] == int(suggest_pred[0]), 'ORGCODE'].iloc[0]
    onemore_code = recommend_code_map.loc[recommend_code_map['AICODE'] == int(onemore_pred[0]), 'ORGCODE'].iloc[0]

    result = {
        'code': 0,
        'message': '',
        'data': {
            'recommend': suggest_code,
            'special_offer': onemore_code
        }
    }

    print('result:', result)

    return jsonify(result)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5050, debug=True)


