Select * From InteriorStyleInfo;
Select * From InteriorStyleOptionInfo;
Select * From InteriorStyleDetailInfo;
Select * From InteriorStyleConstructionDetailInfo;
Select * From StyleColorComboInfo;

SELECT * FROM AIDesignOptionInfo
WHERE aiDesignInfoID = 22;

Select * From AIDesignReportInfo;


Select adr.accountID,
       adr.aiDesignReportInfoID,
       adr.selectedStyle,
       adr.selectedColorCombo,
#        psi.styleAbbreviation,
       psi.colorComboCode
From AIDesignReportInfo adr
         Join ProposedStyleInfo psi on adr.aiDesignReportInfoID = psi.aiDesignReportInfoID
WHERE adr.accountID = 11;

SELECT PSI.styleAbbreviation,
       ADRI.aiDesignInfoID
FROM ProposedStyleInfo PSI
         JOIN AIDesignReportInfo ADRI on PSI.aiDesignReportInfoID = ADRI.aiDesignReportInfoID
#          JOIN AIDesignOptionInfo ADOI on ADRI.aiDesignInfoID = ADOI.aiDesignInfoID;
