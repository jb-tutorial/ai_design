import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from xgboost import XGBClassifier

dirname = os.path.dirname(__file__)
excel_file = os.path.join(dirname, r'data_211221.xlsx')

def make_model():
    df = pd.read_excel(excel_file)
    target_suggest = df['rate1'] > 0.4
    x_data_suggest = df.loc[target_suggest, 'PR_ST':'FA_CO']
    y_data_suggest = df.loc[target_suggest, 'suggest']
    # x_data_onemore = df.loc[df['rate2'] > 0.8, 'style':'C6']
    # y_data_onemore = df.loc[df['rate2'] > 0.8, 'onemore']

    x_train_suggest, x_test_suggest, y_train_suggest, y_test_suggest = train_test_split(
        x_data_suggest,
        y_data_suggest,
        test_size=0.2, shuffle=True, random_state=20)

    xgb = XGBClassifier(n_estimators=50, max_depth=3)
    xgb.fit(x_train_suggest, y_train_suggest)

    y_dtc_pred = xgb.predict(x_test_suggest)
    # y_dtc_pred = xgb.predict(x_test_suggest.head(10))
    # y_dtc_pred = xgb.predict(x_test_suggest.iloc[0, :])
    print('예측값:', y_dtc_pred)

    dtc_acc = accuracy_score(y_test_suggest, y_dtc_pred)
    print('Accuracy:%.4f' % dtc_acc)

    # return xgb
    return 'ok'


make_model()

