import os

import pandas as pd

dirname = os.path.dirname(__file__)

csv_file = os.path.join(dirname, r'recom_code.csv')
recommend_code_map = pd.read_csv(csv_file)

# org_code = recommend_code_map.loc[recommend_code_map['AICODE'] == 609, 'ORGCODE']

org_code = recommend_code_map.loc[recommend_code_map['AICODE'] == 301, 'ORGCODE'].iloc[0]

