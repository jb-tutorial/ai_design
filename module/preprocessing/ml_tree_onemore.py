import os

import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

# gets your current directory
dirname = os.path.dirname(__file__)
excel_file = os.path.join(dirname, r'../data_211219.xlsx')

df = pd.read_excel(excel_file)
x_data_onemore = df.loc[df['rate2'] > 0.2, 'style':'C6']
y_data_onemore = df.loc[df['rate2'] > 0.2, 'onemore']

x_train_onemore, x_test_onemore, y_train_onemore, y_test_onemore = train_test_split(
    x_data_onemore,
    y_data_onemore,
    test_size=0.2, shuffle=True, random_state=20)

dtc = DecisionTreeClassifier(n_estimators=50, max_depth=3)
dtc.fit(x_train_onemore, y_train_onemore)

y_dtc_pred = dtc.predict(x_test_onemore)
print('특별한 제안 예측값:', y_dtc_pred)

dtc_acc = accuracy_score(y_test_onemore, y_dtc_pred)
print('Accuracy:%.4f' % dtc_acc)

