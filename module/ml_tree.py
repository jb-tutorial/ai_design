import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

# gets your current directory
dirname = os.path.dirname(__file__)
excel_file = os.path.join(dirname, r'data_211219.xlsx')

df = pd.read_excel(excel_file)
x_data_suggest = df.loc[df['rate1'] > 0.6, 'style':'C6']
y_data_suggest = df.loc[df['rate1'] > 0.6, 'suggest']
x_data_onemore = df.loc[df['rate2'] > 0.8, 'style':'C6']
y_data_onemore = df.loc[df['rate2'] > 0.8, 'onemore']

x_train_suggest, x_test_suggest, y_train_suggest, y_test_suggest = train_test_split(
    x_data_suggest,
    y_data_suggest,
    test_size=0.2, shuffle=True, random_state=20)

dtc = DecisionTreeClassifier(max_depth=3, random_state=20)
dtc.fit(x_train_suggest, y_train_suggest)

y_dtc_pred = dtc.predict(x_test_suggest)
print('예측값:', y_dtc_pred[:5])

dtc_acc = accuracy_score(y_test_suggest, y_dtc_pred)
print('Accuracy:%.4f' % dtc_acc)

# data_suggestion.duplicated().sum()
# x_train = data.loc
#
# plt.figure(figsize=(20, 20))
# sns.set(font_scale=0.8)
# sns.heatmap(df_major.corr(), annot=True, cbar=True)


# df.info()
#
